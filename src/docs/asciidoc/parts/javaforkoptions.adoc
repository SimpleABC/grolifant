== Java Fork Options

There are a number of places in the Gradle API which utilises `JavaForkOptions`, but there is no easy way for a plugin provider to create a set of Java options for later usage. For this purpose we have created a version that looks the same and implements most of the methods on the interface.

Here is an example of using it with a Gradle worker configuration.

[source,groovy]
----
JavaForkOptions jfo = new JavaForkOptions()

jfo.systemProperties 'a.b.c' : 1

workerExecutor.submit(RunnableWorkImpl.class) { WorkerConfiguration conf ->

    forkOptions { org.gradle.process.JavaForkOptions options ->
        jfo.copyTo(options)
    }
}
----
