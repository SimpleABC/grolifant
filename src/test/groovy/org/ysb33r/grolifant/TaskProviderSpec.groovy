/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.grolifant

import org.gradle.api.Action
import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.tasks.Copy
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant.api.TaskProvider
import spock.lang.Specification

import static org.ysb33r.grolifant.api.TaskProvider.*

class TaskProviderSpec extends Specification {

    Project project = ProjectBuilder.builder().build()

    void 'Can register a task'() {
        when:
        TaskProvider provider = registerTask(project, 'foo')

        then:
        provider.name == 'foo'

        when:
        provider.configure {
            group 'space'
        }

        then:
        noExceptionThrown()
    }

    void 'Can register a task with a type'() {
        when:
        TaskProvider<Copy> provider = registerTask(project, 'foo', Copy)

        then:
        provider.name == 'foo'

        when:
        provider.configure {
            into 'space'
        }

        then:
        noExceptionThrown()
    }

    void 'Can register a task with arguments'() {
        when:
        TaskProvider provider = registerTask(project, 'foo', MyTask, 'a', 'b')

        then:
        provider.name == 'foo'

        when:
        provider.configure {
            into 'space'
        }

        then:
        noExceptionThrown()
    }

    void 'Can register a task with closure'() {
        when:
        TaskProvider provider = registerTask(project, 'foo',Copy) {
            into 'bar'
        }

        then:
        provider.name == 'foo'

        when:
        provider.configure {
            into 'space'
        }

        then:
        noExceptionThrown()
    }

    void 'Can register a task with action'() {
        when:
        TaskProvider provider = registerTask(project, 'foo',Copy, new Action<Task>() {
            @Override
            void execute(Task task) {
                task.group = 'foo'
            }
        })

        then:
        provider.name == 'foo'

        when:
        provider.configure {
            into 'space'
        }

        then:
        noExceptionThrown()
    }

    void 'Can find a task by name if created'() {
        when:
        project.tasks.create('foo', Copy)

        then:
        taskByName(project, 'foo') != null
        taskByName(project, 'foo') instanceof TaskProvider
    }

    void 'Can find a task by name if registered'() {
        when:
        project.tasks.register('foo', Copy)

        then:
        taskByName(project, 'foo') != null
        taskByName(project, 'foo') instanceof TaskProvider
    }

    void 'Can find a task by name and type'() {
        when:
        project.tasks.register('foo', Copy)

        then:
        taskByTypeAndName(project, Copy, 'foo') != null

    }

    void 'Can configure a task by name'() {
        when:
        registerTask(project, 'foo', Copy)
        configureByName(project, 'foo') {
            into 'space'
        }

        then:
        noExceptionThrown()
    }

    void 'Can configure tasks of a specific type'() {
        when:
        registerTask(project, 'foo', Copy)
        configureEach(project, Copy) {
            into 'space'
        }

        then:
        noExceptionThrown()
    }

    static class MyTask extends DefaultTask {
        MyTask(String a, String b) {
            super()
        }
    }
}

