/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.grolifant

import org.ysb33r.grolifant.api.Version
import spock.lang.Specification
import spock.lang.Unroll

class VersionSpec extends Specification {

    @Unroll
    void '#ver is a valid version string'() {

        when:
        Version v = Version.of(ver)

        then:
        verifyAll {
            v.major == major
            v.minor == minor
            v.patch == patch
            v.alpha == alpha
            v.beta == beta
            v.rc == rc
            v.milestone == milestone
            v.snapshot == snapshot
        }

        where:
        ver                         || major | minor | patch | alpha | beta  | rc    | milestone | snapshot
        '11.22'                     || 11    | 22    | null  | false | false | false | false     | false
        '11.22.33'                  || 11    | 22    | 33    | false | false | false | false     | false
        '1.45-alpHa'                || 1     | 45    | null  | true  | false | false | false     | false
        '1.45-alpha.8'              || 1     | 45    | null  | true  | false | false | false     | false
        '1.56-Alpha_8'              || 1     | 56    | null  | true  | false | false | false     | false
        '1.2.3-Beta123'             || 1     | 2     | 3     | false | true  | false | false     | false
        '4.5.6-RC3'                 || 4     | 5     | 6     | false | false | true  | false     | false
        '1.2.3-M4'                  || 1     | 2     | 3     | false | false | false | true      | false
        '1.2.3-SNAPSHOT'            || 1     | 2     | 3     | false | false | false | false     | true
        '1.2.3-12345678901234'      || 1     | 2     | 3     | false | false | false | false     | true
        '1.2.3-12345678901234-9999' || 1     | 2     | 3     | false | false | false | false     | true
        '1.2.3-12345678901234+9999' || 1     | 2     | 3     | false | false | false | false     | true
        '1.22-ALPHA_8-SNAPSHOT'     || 1     | 22    | null  | true  | false | false | false     | true
    }

    @Unroll
    void 'Version #v1 is less than #v2'() {

        expect:
        Version.of(v1) < Version.of(v2)

        where:
        v1                         | v2
        '1.12'                     | '12.13'
        '1.12'                     | '2.1'
        '1.12'                     | '1.12.1'
        '1.12-alpha.1'             | '1.12-alpha.2'
        '1.12-alpha.8'             | '1.12-beta.1'
        '1.12-alpha.8'             | '1.12-rc.1'
        '1.12-alpha.8'             | '1.12-M1'
        '1.12-beta.8'              | '1.12-rc.1'
        '1.12-M8'                  | '1.12-rc.1'
        '1.12-rc.1'                | '1.12'
        '1.12-SNAPSHOT'            | '1.12'
        '1.12-12345678901234'      | '1.12'
        '1.12-12345678901234-9999' | '1.12'
        '1.12-12345678901234+9999' | '1.12'
        '1.12-SNAPSHOT'            | '1.12-12345678901234'
        '1.12-alpha'               | '1.12-alpha.1'
    }
}