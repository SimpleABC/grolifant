/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.grolifant.api

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.file.CopySpec
import org.gradle.api.file.FileCollection
import org.ysb33r.grolifant.internal.ClassLocationImpl
import org.ysb33r.grolifant.internal.Transform
import org.ysb33r.grolifant.internal.copyspec.Resolver

import java.nio.file.Path
import java.nio.file.Paths
import java.util.regex.Pattern

@CompileStatic
class FileUtils {

    static final Pattern SAFE_FILENAME_REGEX = ~/[^\w_\-.$]/

    /** Converts a string into a string that is safe to use as a file name. T
     *
     * The result will only include ascii characters and numbers, and the "-","_", #, $ and "." characters.
     *
     * @param A potential file name
     * @return A name that is safe on the local filesystem of the current operating system.
     */
    @CompileDynamic
    static String toSafeFileName(String name) {
        name.replaceAll SAFE_FILENAME_REGEX, { String match ->
            String bytes = match.bytes.collect { int it -> Integer.toHexString(it) }.join('')
            "#${bytes}!"
        }
    }

    /** Converts a collection of String into a {@link Path} with all parts guarantee to be safe file parts
     *
     * @param parts File path parts
     * @return File path
     * @since 0.8
     */
    static Path toSafePath(String... parts) {
        List<String> safeParts = Transform.toList(parts as List) { String it -> toSafeFileName(it) }
        safeParts.size() > 0 ? Paths.get(safeParts[0], safeParts[1..-1].toArray() as String[]) : Paths.get(safeParts[0])
    }

    /** Converts a collection of String into a {@@link File} with all parts guarantee to be safe file parts
     *
     * @param parts File path parts
     * @return File path
     * @since 0.8
     */
    static File toSafeFile(String... parts) {
        toSafePath(parts).toFile()
    }

    /** Returns the file collection that a {@link CopySpec} describes.
     *
     * @param copySpec An instance of a {@link CopySpec}
     * @return Result collection of files.
     */
    static FileCollection filesFromCopySpec(CopySpec copySpec) {
        Resolver.resolveFiles(copySpec)
    }

    /** Provides a list of directories below another directory
     *
     * @param distDir Directory
     * @return List of directories. Can be empty if, but never {@code null}
     *   supplied directory.
     */
    static List<File> listDirs(File distDir) {
        if (distDir.exists()) {
            distDir.listFiles(new FileFilter() {
                @Override
                boolean accept(File pathname) {
                    pathname.directory
                }
            }) as List<File>
        } else {
            []
        }
    }

    /** Returns the classpath location for a specific class
     *
     * @param aClass Class to find.
     * @return Location of class. Can be {@code null} which means class has been found, but cannot be placed
     *   on classpath
     * @throw ClassNotFoundException* @since 0.9
     */
    @SuppressWarnings('DuplicateStringLiteral')
    static ClassLocation resolveClassLocation(Class aClass) {
        String location = aClass?.protectionDomain?.codeSource?.location

        if (location) {
            new ClassLocationImpl(new File(location.toURI()).absoluteFile)
        } else {
            URI uri = aClass.getResource(
                '/' + aClass.canonicalName.replace('.', '/') + '.class'
            )?.toURI()

            if (uri == null) {
                throw new ClassNotFoundException("Location for ${aClass.name} cannot be located.")
            } else if (uri.scheme == 'jar') {
                new ClassLocationImpl(
                    new File(new URI(uri.rawSchemeSpecificPart.replaceAll(~/(!.+?)$/, ''))).absoluteFile
                )
            } else if (uri.scheme == 'jrt') {
                new ClassLocationImpl(uri.toURL())
            } else {
                new ClassLocationImpl(new File(uri).parentFile.absoluteFile)
            }
        }
    }

    /** Returns the project cache directory for the given project.
     *
     * @param project Project to query.
     *
     * @return Project cache directory. Never {@code null}.
     *
     * @since 0.14
     */
    static File projectCacheDirFor(Project project) {
        project.gradle.startParameter.projectCacheDir ?: project.file("${project.rootDir}/.gradle")
    }
}