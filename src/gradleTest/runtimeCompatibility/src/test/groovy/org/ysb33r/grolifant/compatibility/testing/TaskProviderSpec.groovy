/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.testing

import org.gradle.api.Project
import org.gradle.api.tasks.Copy
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant.api.TaskProvider
import spock.lang.Specification

import static org.ysb33r.grolifant.api.TaskProvider.registerTask
import static org.ysb33r.grolifant.api.TaskProvider.taskByName

class TaskProviderSpec extends Specification {
    Project project = ProjectBuilder.builder().build()

    void 'Register a task'() {
        when:
        // tag::register_task[]
        TaskProvider tp = registerTask(project, 'foo', Copy) { // <1>
            into 'foo'
        }
        // end::register_task[]

        // tag::configure_task[]
        tp.configure { // <2>
            from 'bar'
        }
        // end::configure_task[]

        then:
        taskByName(project, 'foo').name == 'foo'

    }
}