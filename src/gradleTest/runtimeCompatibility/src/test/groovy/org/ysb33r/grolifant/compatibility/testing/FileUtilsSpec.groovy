/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.testing

import org.gradle.api.Project
import org.gradle.api.file.CopySpec
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant.api.FileUtils
import spock.lang.Specification
import spock.lang.Unroll

class FileUtilsSpec extends Specification {

    @Unroll
    void 'Safe file name: #name'() {

        expect:
        safe == FileUtils.toSafeFileName(name)

        where:
        name           || safe
        'abc'          || 'abc'
        'a.bc_d-e$f'   || 'a.bc_d-e$f'
        'a@b!c&e'      || 'a#40!b#21!c#26!e'
    }

    void 'Extract file collection from copy specification'() {
        given: 'some files in a source directory'
        Project project = ProjectBuilder.builder().build()
        File srcDir = new File(project.projectDir,'src')
        srcDir.mkdirs()
        new File(srcDir,'1.txt').text='123'
        new File(srcDir,'2.txt').text='456'

        when: 'a copy specification is created'
        CopySpec cs = project.copySpec {
            from srcDir
            include '**/*.txt'
        }

        and: 'a file collection is requested'
        Set<String> files = FileUtils.filesFromCopySpec(cs).files*.name

        then: 'the files in the copy specification should be listed'
        files.contains('1.txt')
        files.contains('2.txt')
    }
}