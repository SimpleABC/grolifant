/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.testing

import org.ysb33r.grolifant.api.StringUtils
import spock.lang.Specification

class ProviderSpec extends Specification {

    void 'StringUtils can convert a provider'() {
        given:
        Object provider = getTestProvider('foo')

        expect:
        StringUtils.stringize(provider) == 'foo'
    }

    Object getTestProvider(final String value) {
        [
            get      : { -> value },
            getOrNull: { -> value },
            getOrElse: { -> value },
            map      : { null },
            flatMap  : { null },
            isPresent: { -> true }
        ] as org.gradle.api.provider.Provider
    }
}